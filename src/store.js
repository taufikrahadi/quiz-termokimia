import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        quiz: [
            {
                question: '',
                img: require('./assets/soal/nomor1.jpg'),
                answers: [
                    {
                        id: 'a',
                        answer: '-3.139kJ',
                    },
                    {
                        id: 'b',
                        answer: '+3.139kJ',
                    },
                    {
                        id: 'c',
                        answer: '-685kJ',
                    },
                    {
                        id: 'd',
                        answer: '+685kJ',
                    },
                    {
                        id: 'e',
                        answer: '-587kJ',
                    }
                ],
                correctAnswer: 'a'
            },
            {
                question: '',
                img: require('./assets/soal/nomor2.jpg'),
                answers: [
                    {
                        id: 'a',
                        answer: 'λH Penguraian CO = -250,8 kJ'
                    },
                    {
                        id: 'b',
                        answer: 'λH Pembentukan CO2 = -250,8 kJ'
                    },
                    {
                        id: 'c',
                        answer: 'λH Pembakaran CO = -250,8 kJ'
                    },
                    {
                        id: 'd',
                        answer: 'λH Penguraian CO = -250,8 kJ'
                    },
                    {
                        id: 'e',
                        answer: 'λH Reaksi 2 mol CO dan 1 mol O2 = -250,8 kJ'
                    },
                ],
                correctAnswer: 'a'
            },
            {
                question: '',
                img: require('./assets/soal/nomor3.jpg'),
                answers: [
                    {
                        id: 'a',
                        answer: '+168 kJ'
                    },
                    {
                        id: 'b',
                        answer: '+60 kJ'
                    },
                    {
                        id: 'c',
                        answer: '143 kJ'
                    },
                    {
                        id: 'd',
                        answer: '-168 kJ'
                    },
                    {
                        id: 'e',
                        answer: '404 kJ'
                    },
                ],
                correctAnswer: 'a'
            },
            {
                question: '',
                img: require('./assets/soal/nomor4.jpg'),
                answers: [
                    {
                        id: 'a',
                        answer: '-1.780 kJ'
                    },
                    {
                        id: 'b',
                        answer: '-445 kJ'
                    },
                    {
                        id: 'c',
                        answer: '+890 kJ'
                    },
                    {
                        id: 'd',
                        answer: '-890 kJ'
                    },
                    {
                        id: 'e',
                        answer: '+445 kJ'
                    },
                ],
                correctAnswer: 'a'
            },
            {
                question: '',
                img: require('@/assets/soal/nomor5.jpg'),
                answers: [
                    {
                        id: 'a',
                        answer: '-953 kJ/mol'
                    },
                    {
                        id: 'b',
                        answer: '-803 kJ/mol'
                    },
                    {
                        id: 'c',
                        answer: '-563 kJ/mol'
                    },
                    {
                        id: 'd',
                        answer: '+803 kJ/mol'
                    },
                    {
                        id: 'e',
                        answer: '+953 kJ/mol'
                    }
                ],
                correctAnswer: 'a'
            },
            {
                question: '',
                img: require('@/assets/soal/nomor6.jpg'),
                answers: [
                    {
                        id: 'a',
                        answer: 'Kalor Pembentukan CO = 2x kJ mol<sup>-1</sup>'
                    },
                    {
                        id: 'b',
                        answer: 'Kalor Penguraian CO = x kJ mol<sup>-1</sup>'
                    },
                    {
                        id: 'c',
                        answer: 'Kalor Pembakaran CO = 2x kJ mol<sup>-1</sup>'
                    },
                    {
                        id: 'd',
                        answer: 'Kalor Pembakaran CO = 1/2x kJ mol<sup>-1</sup>'
                    },
                    {
                        id: 'e',
                        answer: 'Kalor Pembentukan CO = 1/2x kJ mol<sup>-1</sup>'
                    },
                ],
                correctAnswer: 'a'
            },
            {
                question: '',
                img: require('@/assets/soal/nomor7.jpg'),
                answers: [
                    {
                        id: 'a',
                        answer: '109,5 kJ/mol'
                    },
                    {
                        id: 'b',
                        answer: '-109,5 kJ/mol'
                    },
                    {
                        id: 'c',
                        answer: '1.750 kJ/mol'
                    },
                    {
                        id: 'd',
                        answer: '-219 kJ/mol'
                    },
                    {
                        id: 'e',
                        answer: '-291 kJ/mol'
                    },
                ],
                correctAnswer: 'a'
            },
            {
                question: '',
                img: require('@/assets/soal/nomor8.jpg'),
                answers: [
                    {
                        id: 'a',
                        answer: '-546'
                    },
                    {
                        id: 'b',
                        answer: '-273'
                    },
                    {
                        id: 'c',
                        answer: '+273'
                    },
                    {
                        id: 'd',
                        answer: '+546'
                    },
                    {
                        id: 'e',
                        answer: '-1.092'
                    },
                ],
                correctAnswer: 'a'
            },
            {
                question: '',
                img: require('@/assets/soal/nomor9soal.jpg'),
                answers: [
                    {
                        id: 'a',
                        img: require('@/assets/soal/nomor9a.jpg')
                    },
                    {
                        id: 'b',
                        img: require('@/assets/soal/nomor9b.jpg')
                    },
                    {
                        id: 'c',
                        img: require('@/assets/soal/nomor9c.jpg')
                    },
                ],
                correctAnswer: 'a'
            },
            {
                question: 'Menurut hukum Hess, harga x dalam diagram tersebut adalah...',
                img: require('@/assets/soal/10.jpg'),
                answers: [
                    {
                        id: 'a',
                        answer: '-197 kJ'
                    },
                    {
                        id: 'b',
                        answer: '+197 kJ'
                    },
                    {
                        id: 'c',
                        answer: '-1.383 kJ'
                    },
                    {
                        id: 'd',
                        answer: '+1.383 kJ'
                    },
                    {
                        id: 'e',
                        answer: '-1.970 kJ'
                    },
                ],
                correctAnswer: 'a'
            },
            {
                question: '',
                img: require('@/assets/soal/nomor11.jpg'),
                answers: [
                    {
                        id: 'a',
                        answer: 'Hukum Hess telah terbukti'
                    },
                    {
                        id: 'b',
                        answer: 'Kristal KCI memiliki sejumlah energi yang lebih rendah daripada larutan ion-ion K<sup>+</sup> dan C<sup>-</sup>'
                    },
                    {
                        id: 'c',
                        answer: 'reaksi pelarutan tersebut adalah eksoterm'
                    },
                    {
                        id: 'd',
                        answer: 'KCI berikatan ionik'
                    },
                    {
                        id: 'e',
                        answer: 'KCI bersifat elektrolit'
                    },
                ],
                correctAnswer: 'a'
            },
            {
                question: '',
                img: require('@/assets/soal/nomor12.jpeg'),
                answers: [
                    {
                        id: 'a',
                        answer: 'Berguna untuk melancarkan segala aktifitas reaksi'
                    },
                    {
                        id: 'b',
                        answer: 'Membantu kalor berjalan saat reaksi berlangsung'
                    },
                    {
                        id: 'c',
                        answer: 'Karna kalor yang berlangsung sangat berguna makanya digunakan hukum Hess'
                    },
                    {
                        id: 'd',
                        answer: 'Hukum ini sangat berguna karena kenyataannya tidak semua reaksi dapat ditentukan kalor reaksi nya secara eksperimen'
                    },
                    {
                        id: 'e',
                        answer: 'Sangat berguna bagi setiap atom-atom yang ada di alam semesta'
                    }
                ],
                correctAnswer: 'a'
            },
            {
                question: '',
                img: require('@/assets/soal/nomor13.jpeg'),
                answers: [
                    {
                        id: 'a',
                        answer: 'Hanya bergantung pada keadaan awal dan akhir reaksi'
                    },
                    {
                        id: 'b',
                        answer: 'Hanya bergantung pada keadaan awal saja',
                    },
                    {
                        id: 'c',
                        answer: 'Hanya bergantung pada keadaan normal saat reaksi berlangsung'
                    },
                    {
                        id: 'd',
                        answer: 'Hanya bergantung pada saat reaksi berlangsung lama'
                    },
                    {
                        id: 'e',
                        answer: 'Hanya dapat bergantung pada saat reaksi berlangsung cepat'
                    }
                ],
                correctAnswer: 'a'
            },
            {
                question: '',
                img: require('@/assets/soal/nomor14.jpg'),
                answers: [
                    {
                        id: 'a',
                        answer: '1173 kJ/mol'
                    },
                    {
                        id: 'b',
                        answer: '925,5 kJ/mol'
                    },
                    {
                        id: 'c',
                        answer: '804,5 kJ/mol'
                    },
                    {
                        id: 'd',
                        answer: '586,5 kJ/mol'
                    },
                    {
                        id: 'e',
                        answer: '402,25 kJ/mol'
                    },
                ]
            },
            {
                question: '',
                img: require('@/assets/soal/nomor15.jpg'),
                answers: [
                    {
                        id: 'a',
                        answer: '-694,3 kJ'
                    },
                    {
                        id: 'b',
                        answer: '-236,6 kJ'
                    },
                    {
                        id: 'c',
                        answer: '236,6 kJ'
                    },
                    {
                        id: 'd',
                        answer: '-130,3 kJ'
                    },
                    {
                        id: 'e',
                        answer: '694,3 kJ'
                    },
                ],
                correctAnswer: 'a'
            },
            {
                question: '',
                img: require('@/assets/soal/nomor16.jpg'),
                answers: [
                    {
                        id: 'a',
                        answer: '1.110 kJ'
                    },
                    {
                        id: 'b',
                        answer: '248 kJ'
                    },
                    {
                        id: 'c',
                        answer: '-183 kJ'
                    },
                    {
                        id: 'd',
                        answer: '-248 kJ'
                    },
                    {
                        id: 'e',
                        answer: '-496 kJ'
                    },
                ],
                correctAnswer: 'a'
            },
            {
                question: '',
                img: require('@/assets/soal/nomor17.jpg'),
                answers: [
                    {
                        id: 'a',
                        answer: '848,5 kJ'
                    },
                    {
                        id: 'b',
                        answer: '212,13 kJ'
                    },
                    {
                        id: 'c',
                        answer: '424,25 kJ'
                    },
                    {
                        id: 'd',
                        answer: '106,06 kJ'
                    },
                    {
                        id: 'e',
                        answer: '1.697kJ'
                    },
                ],
                correctAnswer: 'a'
            },
            {
                question: '',
                img: require('@/assets/soal/nomor18.jpg'),
                answers: [
                    {
                        id: 'a',
                        answer: '1.173,0 kJ'
                    },
                    {
                        id: 'b',
                        answer: '586,5 kJ'
                    },
                    {
                        id: 'c',
                        answer: '391,0 kJ'
                    },
                    {
                        id: 'd',
                        answer: '195,5 kJ'
                    },
                    {
                        id: 'e',
                        answer: '159,5 kJ'
                    },
                ],
                correctAnswer: 'a'
            },
            {
                question: '',
                img: require('@/assets/soal/nomor19.jpg'),
                answers: [
                    {
                        id: 'a',
                        answer: '8 kJ'
                    },
                    {
                        id: 'b',
                        answer: '121 kJ'
                    },
                    {
                        id: 'c',
                        answer: '222 kJ'
                    },
                    {
                        id: 'd',
                        answer: '242 kJ'
                    },
                    {
                        id: 'e',
                        answer: '484 kJ'
                    },
                ],
                correctAnswer: 'a'
            }
        ],
        userName: '',
        correctAnswer: 0,
        time: 900,
        id: 0,
    },

    mutations: {
        setUserName(state, data) {
            localStorage.setItem('username', data)
        },

        setCorrectAnswer(state) {
            state.correctAnswer += 1
        },

        plusId(state) {
            state.id += 1
        },

        reduceId(state) {
            state.id -= 1
        },

        setQuizTime(state) {
            state.time -= 1
        },
        reduceQuizTime(state) {
            state.time -= 1
        }
    },

    actions: {
        
    },

    getters: {
        checkQuizPosition(state, id) {
            if(state.quiz.length >= id) {
                return true
            }

            return false
        },

        points(state) {
            return parseInt(state.correctAnswer * 5.55555555556)
        },

        checkTimes(state) {
            if(state.time > 0) {
                return true
            }

            return false
        }
    }
})

export default store