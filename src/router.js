import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import Home from './components/Home'
import StartQuiz from './components/StartQuiz'
import Quiz from './components/Quiz'

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home,
    },
    {
        path: '/start',
        name: 'start quiz',
        component: StartQuiz
    },
    {
        path: '/quiz',
        name: 'quiz',
        component: Quiz,
    },
    {
        path: '/finish',
        name: 'after quiz',
        component: () => import('./components/Finish')
    }
]

const router = new VueRouter({
    mode: 'history',
    routes
})

export default router